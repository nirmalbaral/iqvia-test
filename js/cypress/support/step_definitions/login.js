given("the user has the correct credentials", () => {
  cy.visit('/');
});

when(/^the user enters username "(.*?)"$/, username => {
  cy.get('input[name="email"]').type(username);
});

then(/^the user enters password "(.*?)"$/, password => {
  cy.get('input[name="password"]').type(password);
});

then("clicks Login", () => {
  cy.get('button[aria-label="login"]').click();
});

then("the user is presented with a welcome message", () => {
  cy.get('article.content').should('contain', 'Welcome Dr I Test');
});

given("the user has the incorrect credentials", () => {
  cy.visit('/');
});

then("the user is presented with a error message", () => {
  cy.get('#login-error-box').should('contain', 'Credentials are incorrect');
});
